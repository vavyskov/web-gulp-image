# WEB gulp image

Pomocný nástroj příkazového řádku pro vytváření náhledů a komprimaci obrázků.

## Adresářová struktura

Popis adresářové struktury:

```bash
public/
    assets/
        images/
            resize/
src/
    assets/
        images/
```

### Zdroj

- **src/assets/images** - originální obrázky

### Cíl

- **public/assets/images** - zkomprimované obrázky
- **public/assets/images/resize** - zkomprimované vygenerované náhledy obrázků:
  - xs: šířka 480 px
  - sm: šířka 576 px
  - md: šířka 768 px
  - lg: šířka 992 px
  - xl: šířka 1200 px
  - xxl: šířka 1440 px
  - fhd: šířka 1920 px
  - qhd: šířka 2560 px
  - uhd: šířka 3840 px
  - fuhd: šířka 7680 px
  - quhd: šířka 15360 px

Před vygenerování obrázků je obsah adresáře **public** nejprve vymazán.

## Stažení

```bash
git clone git@gitlab.com:vavyskov/web-gulp-image.git
```

Případně stažení **ZIP** archivu.


## Instalace

1. Nainstalujte [Node.js](https://nodejs.org/en/) - obsahuje správce balíčků **npm**.
	- Verze:
	  - **Current** (nejnovější verze)
	  - **LTS** (verze s dlouhodobou podporou) 
	- Test funkčnosti: `npm --version`
1. Nainstalujte [ImageMagick](http://www.imagemagick.org/script/index.php) **včetně nástroje convert** pro prácí s obrázky v příkazovém řádku (vyžaduje **znovupřihlášení** uživatele, případně **restart** počítače).
	- Test funkčnosti: `convert --version`
1. Přejděte do adresáře **web-gulp-image/**.
1. Nainstalujte požadované *npm* balíčky a jejich závislosti:

    ```bash
    npm install
    ```
1. Volitelně nalinkujte příkaz *gulp* (umožňuje spouštět samostatně spustitelné úlohy):

    ```bash
    npm link gulp
    ```

## Možnosti

Rychlé vygenerování obrázků (základní komprese)

```bash
npm run fast-build
```

Vygenerování obrázků s maximální kompresí:

```bash
npm run build
```

---

## Samostatně spustitelné úlohy

Samostatně spustitelné úlohy definované v souboru `gulpfile.js`:

```bash
gulp clean
gulp img --mode development
gulp img
gulp resizeImage --mode development
gulp resizeImage
gulp build --mode development
gulp build
```
