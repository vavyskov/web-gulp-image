'use strict';

// Packages
var gulp = require('gulp'),
    del = require('del'),
    mergeStream = require("merge-stream"),
    imageResize = require('gulp-image-resize'),
    rename = require('gulp-rename'),
    imagemin = require('gulp-imagemin'), // Quick development
    image = require('gulp-image'), // Pruduction - better compression than gulp-imagemin!!!
    gulpif = require('gulp-if'),
    argv = require('yargs').argv;

// Development mode
var minify = true;
if (argv.mode === 'development') {
    minify = false;
}

// Clean (Delete folders)
function clean() {
    return del([
        'public/**/*',
        '!public/README.md',
    ]);
}

// Image (Optimise PNG, JPG, GIF nad SVG images)
function img() {
    return gulp.src('src/assets/images/**/*.{jpg,png,gif,svg}')
        .pipe(gulpif(minify, image(), imagemin()))
        .pipe(gulp.dest('public/assets/images/'));
}

// Resize (Generate thumbnail - Require ImageMagick "convert")
function resizeImage() {
    var resizeXs = gulp.src('src/assets/images/**/*.{jpg,png,gif}')
        .pipe(imageResize({
            width: 480,
            imageMagick: true
        }))
        .pipe(rename({
            suffix: '-xs'
        }))
        .pipe(gulpif(minify, image(), imagemin()))
        .pipe(gulp.dest('public/assets/images/resize/'));
    var resizeSm = gulp.src('src/assets/images/**/*.{jpg,png,gif}')
        .pipe(imageResize({
            width: 576,
            imageMagick: true
        }))
        .pipe(rename({
            suffix: '-sm'
        }))
        .pipe(gulpif(minify, image(), imagemin()))
        .pipe(gulp.dest('public/assets/images/resize/'));
    var resizeMd = gulp.src('src/assets/images/**/*.{jpg,png,gif}')
        .pipe(imageResize({
            width: 768,
            imageMagick: true
        }))
        .pipe(rename({
            suffix: '-md'
        }))
        .pipe(gulpif(minify, image(), imagemin()))
        .pipe(gulp.dest('public/assets/images/resize/'));
    var resizeLg = gulp.src('src/assets/images/**/*.{jpg,png,gif}')
        .pipe(imageResize({
            width: 992,
            imageMagick: true
        }))
        .pipe(rename({
            suffix: '-lg'
        }))
        .pipe(gulpif(minify, image(), imagemin()))
        .pipe(gulp.dest('public/assets/images/resize/'));
    var resizeXl = gulp.src('src/assets/images/**/*.{jpg,png,gif}')
        .pipe(imageResize({
            width: 1200,
            imageMagick: true
        }))
        .pipe(rename({
            suffix: '-xl'
        }))
        .pipe(gulpif(minify, image(), imagemin()))
        .pipe(gulp.dest('public/assets/images/resize/'));
    var resizeXxl = gulp.src('src/assets/images/**/*.{jpg,png,gif}')
        .pipe(imageResize({
            width: 1440,
            imageMagick: true
        }))
        .pipe(rename({
            suffix: '-xxl'
        }))
        .pipe(gulpif(minify, image(), imagemin()))
        .pipe(gulp.dest('public/assets/images/resize/'));
    var resizeFhd = gulp.src('src/assets/images/**/*.{jpg,png,gif}')
        .pipe(imageResize({
            width: 1920,
            imageMagick: true
        }))
        .pipe(rename({
            suffix: '-fhd'
        }))
        .pipe(gulpif(minify, image(), imagemin()))
        .pipe(gulp.dest('public/assets/images/resize/'));
    var resizeQhd = gulp.src('src/assets/images/**/*.{jpg,png,gif}')
        .pipe(imageResize({
            width: 2560,
            imageMagick: true
        }))
        .pipe(rename({
            suffix: '-qhd'
        }))
        .pipe(gulpif(minify, image(), imagemin()))
        .pipe(gulp.dest('public/assets/images/resize/'));
    var resizeUhd = gulp.src('src/assets/images/**/*.{jpg,png,gif}')
        .pipe(imageResize({
            width: 3840,
            imageMagick: true
        }))
        .pipe(rename({
            suffix: '-uhd'
        }))
        .pipe(gulpif(minify, image(), imagemin()))
        .pipe(gulp.dest('public/assets/images/resize/'));
    var resizeFuhd = gulp.src('src/assets/images/**/*.{jpg,png,gif}')
        .pipe(imageResize({
            width: 7680,
            imageMagick: true
        }))
        .pipe(rename({
            suffix: '-fuhd'
        }))
        .pipe(gulpif(minify, image(), imagemin()))
        .pipe(gulp.dest('public/assets/images/resize/'));
    var resizeQuhd = gulp.src('src/assets/images/**/*.{jpg,png,gif}')
        .pipe(imageResize({
            width: 15360,
            imageMagick: true
        }))
        .pipe(rename({
            suffix: '-quhd'
        }))
        .pipe(gulpif(minify, image(), imagemin()))
        .pipe(gulp.dest('public/assets/images/resize/'));
    return mergeStream(resizeXs, resizeSm, resizeMd, resizeLg, resizeXl, resizeXxl, resizeFhd, resizeQhd, resizeUhd, resizeFuhd, resizeQuhd);
}

// Define complex tasks
var build = gulp.series(clean, gulp.parallel(img, resizeImage));

// Export tasks
exports.build = build;
exports.default = build;

exports.clean = clean;
exports.img = img;
exports.resizeImage = resizeImage;
